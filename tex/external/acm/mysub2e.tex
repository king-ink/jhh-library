% This is mysub2e.tex
% NOVEMBER 10 1999
% 2nd rev. 27 JULY 1999 - reflecting use of .eps and .ps only
% 3rd. rev. 10 NOVEMBER 1999 - Corollaries and description env.
% It is an example file showing how to use the 'esub2acm.cls'
% LaTeX2e document class file.
%
% Note this is the file which *contains* the .bbl file.
%
% File finalized by Gerry Murray ACM August 5th. 1999
% murray@hq.acm.org
%


\documentclass{esub2acm}

\begin{document}

\title{A  LaTeX2$_{\textstyle\epsilon}$ Document for Submittal\\ to an ACM Journal}
\author{Ben Trovato\\
        Institute for Clarity in Documentation
    \and
    G.K.M. Tobin}
\sponsor{Association for Computing Machinery, Inc.,
              1515 Broadway, New York, NY 10036, USA
              Tel: (212) 555-1212; Fax: (212) 555-2000}

\begin{abstract}
This paper provides a sample of a LaTeX submission to any of the ACM Journals.  It is intended to supplement the document \textit{Author's Guide for Electronic Submittal of ACM Journal Articles Accepted for Publication: Using \LaTeX$2_\epsilon$\ and BibTeX}.  Accordingly, it is focused on \textit{form} rather than \textit{content}; it is concerned with showing, in the context of an ``actual'' document, the \LaTeX\ commands specifically available for denoting the structure of an article, for eventual
conversion to SGML, rather than with giving rigorous descriptions or explanations of such commands.

To make best use of this document, run it through \LaTeX\ and BibTeX, and
compare this source code with the printed output produced by the dvi file.
\end{abstract}
% A category with only the three required fields
\category{H.4.m} {Information Systems} {Miscellaneous}
%A category including the fourth, optional field
\category{H.32.m} {NonInformation Systems} {Miscellaneous}
    [SGML Database Systems]
\terms{Documentation, Management}
\keywords{SGML,text markup,text tagging }
\begin{bottomstuff}
% You might add a line or two here if a version of your article appeared
% previously, or if the work was supported by an organization or conducted
% under a grant.
\begin{authinfo}
%\name{} use if there are several authors with different addresses
\address{P.O. Box 1221, Dublin, OH 43017-6221}
%\affiliation{} use if there are several authors with different affiliations
%\biography{} optional; not generally used.
\end{authinfo}
% Here's a neat thing:  all you have to put in your document is the command
%"\permission" and LaTeX automatically enters the complete text of the
%"Permission to copy..." boilerplate
\permission
\end{bottomstuff}
\markboth{B. Trovato and G.K.M. Tobin}
     {A Sample LaTeX Document for Submission to an ACM Journal}
\maketitle
\section{Introduction}
This starts the body of the sample article.  Typically, the body is organized
into a hierarchical structure, with numbered or unnumbered headings for sections, subsections, sub-subsections, and even smaller sections.  The
command \texttt{{\char'134}section} that precedes this paragraph is part of
such a hierarchy. \LaTeX\ handles the numbering and placement of these
headings for you, when you use the appropriate heading commands around the titles of the headings.  If you want a sub-subsection or smaller part to be unnumbered in your output, simply append an asterisk to the command name.  Examples of both numbered and unnumbered headings will appear throughout the balance of this sample document.

Because the entire article is contained in the \textbf{document} environment,
you can indicate the start of a new paragraph with a blank line in your
input file; that is why this sentence forms a separate paragraph.

\subsection{Type Changes and Special Characters}
We have already seen several typeface changes in this sample.  You
can indicate italicized words or phrases in your text with the command
\texttt{{\char'134}textit}; emboldening with the command \texttt{{\char'134}textbf}
and typewriter-style (for instance, for computer code) with
\texttt{{\char'134}texttt}.  But remember, you do not have to indicate typestyle changes when such changes are part of the \textit{structural} elements of your article; for instance, the heading of this subsection will be in a sans serif typeface, but that is handled by the document class file. Take care with the use of
the curly braces in typeface changes; they mark the beginning and end of
the text that is to be in the different typeface.

You can use whatever symbols, accented characters, or non-English characters you need anywhere in your document; you can find a complete list of what is
available in the \textit{\LaTeX\ User's Guide}\cite{Lamport:LaTeX}.

\subsection{Math Equations}
You may want to display math equations in three distinct styles: inline,
numbered or non-numbered display.  Each of the three are discussed in the
next sections.

\subsubsection{Inline (In-text) Equations}
A formula that appears in the running text is called an inline or in-text
formula.  It is produced by the \textbf{math} environment, which can be
invoked with the usual \texttt{{\char'134}begin. . .{\char'134}end} construction or with the short form \texttt{\$. . .\$}. You can use any of the symbols and structures, from $\alpha$ to $\omega$, available in
\LaTeX\cite{Lamport:LaTeX}; this section will simply show a few examples of in-text equations in context. Notice how this equation:
\begin{math}\lim_{n\rightarrow \infty}x=0\end{math}, set here in in-line
math style, looks slightly different when set in display style.  (See next
section).

\subsubsection{Display Equations}
A numbered display equation -- one set off by vertical space from the text and centered horizontally -- is produced by the \textbf{equation} environment.
An unnumbered display equation is produced by the \textbf{displaymath} environment.

Again, in either environment, you can use any of the symbols and structures available in \LaTeX; this section will just give a couple of examples of display equations in context. First, consider the equation, shown as an inline equation above:
\begin{equation}\lim_{n\rightarrow \infty}x=0\end{equation}
Notice how it is formatted somewhat differently in the \textbf{displaymath}
environment.  Now, we'll enter an unnumbered equation:
\begin{displaymath}\sum_{i=0}^{\infty} x + 1\end{displaymath}
and follow it with another numbered equation:
\begin{equation}\sum_{i=0}^{\infty}x_i=\int_{0}^{\pi+2} f\end{equation}
just to demonstrate \LaTeX's able handling of numbering.
\subsection{Citations}
Citations to articles or books listed in the Bibliography section of your
article will occur throughout the text of your article.  You should use
BibTeX to automatically produce this bibliography; you simply need to
insert one of several citation commands with a key of the item cited in
the proper location in the \texttt{.tex} file \cite{Lamport:LaTeX}.
The key is a short reference you invent to uniquely identify each work; in this sample document, the key is the first author's surname and a
word from the title.  This identifying key is included with each item
in the \texttt{.bib} file for your article.

The details of the construction of the \texttt{.bib} file are beyond the
scope of this sample document, but more information can be found in the
\textit{Author's Guide}, and exhaustive details in the \textit{\LaTeX\ User's Guide}\cite{Lamport:LaTeX}.

So far, this article has shown only the plainest form of the citation
command, using \texttt{{\char'134}cite}.  You can also use a citation as
a noun in a sentence, as is done here, and in the \citeN{herlihy:methodology} article; use \texttt{{\char'134}citeN} in this case.  You can even say, ``As was
shown in \citeyearNP{bowman:reasoning}. . .'' or ``. . . which
agrees with \citeANP{braams:babel}...'', where the text shows only the
year or only the author component of the citation; use \texttt{{\char'134}citeyearNP} or \texttt{{\char'134}citeANP}, respectively,
for these.  Most of the various citation commands may reference more
than one work\cite{herlihy:methodology,bowman:reasoning}.  A complete
list of all citation commands available is given in the
\textit{Author's Guide}.

\subsection{Tables}
Because tables cannot be split across pages, the best placement for them
is typically the top of the page nearest their initial mention.  To
ensure this proper ``floating'' placement of tables, use the environment
\textbf{table} to enclose the table's contents and the table caption.  The
contents of the table itself must go in the \textbf{tabular} environment, to
be aligned properly in rows and columns, with the desired horizontal and
vertical rules.  Again, detailed instructions on \textbf{tabular} material
is found in the \textit{\LaTeX\ User's Guide}.

Immediately following this sentence is the point at which
the  Table 1 is included in the input file; compare the placement of the
table here with the table in the printout dvi output of this document.
\begin{table}
\centering
\caption{Frequency of Special Characters}
\begin{tabular}{|c|c|l|} \hline
Non-English or Math Characters&Frequency&Comments\\ \hline
\O & 1 in 1,000& Useful for Swedish names\\ \hline
$\pi$ & 1 in 5& Very common in math\\ \hline
\$ & 4 in 5 & Used in business\\ \hline
$\Psi^2_1$ & 1 in 40,000& Unexplained usage\\
\hline\end{tabular}
\end{table}

\subsection{Figures}
Like tables, figures cannot be split across pages; the best placement for them
is typically the top of the page nearest their initial mention.  To
ensure this proper ``floating'' placement of tables, use the environment
\textbf{figure} to enclose the figure and its caption.

This sample document contains examples of \textbf{.eps} and \textbf{.ps}
files to be
displayable with \LaTeX.  More details on each of these is found in
the \textit{Author's Guide}.

\begin{figure}
\centering
\epsfig{file=fly.eps}
\caption{A sample black and white graphic (.eps format).}
\end{figure}

\begin{figure}
\centering
\epsfig{file=fly.eps, height=1in, width=1in}
\caption{A sample black and white graphic (.eps format) that has been resized with the \texttt{epsfig} command.}
\end{figure}

\begin{figure}
\centering
\psfig{file=rosette.ps, height=1in, width=1in}
\caption{A sample black and white graphic (.ps format) that has been resized with the \texttt{psfig} command.}
\end{figure}

\subsection{Theorem-like Constructs}
Other common constructs that may occur in your article are the forms for
logical constructs like theorems, axioms, corollaries and proofs.  There are
two forms, one produced by the command {\tt{\char'134}newtheorem} and the
other by the command {\tt{\char'134}newdef}; perhaps the clearest and easiest way to distinguish them is to compare the two in the output of this sample document:

This uses the {\bf theorem} environment, created by the {\tt{\char'134}newtheorem} command:
\newtheorem{theorem}{Theorem}
\begin{theorem}
Let $f$ be continuous on $[a,b]$.  If $G$ is an antiderivative for $f$ on $[a,b]$, then \begin{displaymath}\int^b_af(t)dt = G(b) - G(a).\end{displaymath}
\end{theorem}

The other uses the {\bf definition} environment, created by the {\tt{\char'134}newdef} command:
\newdef{definition}{Definition}
\begin{definition}
If $z$ is irrational, then by $e^z$ we mean the unique number which has
logarithm $z$: \begin{displaymath}{\log_e^z = z}\end{displaymath}
\end{definition}

Two lists of constructs that use one of these forms is given in the
{\it Author's  Guidelines}.

\newtheorem{corollary}{Corollary}
\begin{corollary}[Fault-Sender]
This is a corollary.
\end{corollary}

\begin{theorem}
Let $f$ be continuous on $[a,b]$.  If $G$ is an antiderivative for $f$ on $[a,b]$, then \begin{displaymath}\int^b_af(t)dt = G(b) - G(a).\end{displaymath}
\end{theorem}

There is one other similar construct environment, which is already set up
for you; i.e. you must {\it not} use a {\tt{\char'134}newdef} command to
create it: the {\bf proof} environment.  Here is a example of its use:
\begin{proof}
Suppose on the contrary there exists a real number $L$ such that
\begin{displaymath}
\lim_{x\rightarrow\infty} \frac{f(x)}{g(x)} = L.
\end{displaymath}
Then
\begin{displaymath}
l=\lim_{x\rightarrow c} f(x) = \lim_{x\rightarrow c} \left[ g{x} \cdot \frac{f(x)}{g(x)} \right ]
= \lim_{x\rightarrow c} g(x) \cdot \lim_{x\rightarrow c} \frac{f(x)}{g(x)} = 0\cdot L
= 0,.
\end{displaymath}
which contradicts our assumption that $l\neq 0$.
\end{proof}

\begin{theorem}
Let $f$ be continuous on $[a,b]$.  If $G$ is an antiderivative for $f$ on $[a,b]$, then \begin{displaymath}\int^b_af(t)dt = G(b) - G(a).\end{displaymath}
\end{theorem}

\begin{corollary}[Fault-Believer]
This is a corollary.
\end{corollary}

Complete rules about using these environments and using the two different
creation commands are in the {\it Author's Guidelines}; please consult them
for more detailed instructions.  If you need to use another construct, not listed therein, which you want to have the same formattingas the Theorem
or the Definition\cite{salas:calculus} shown above, use the {\tt{\char'134}newtheorem} or the {\tt{\char'134}newdef} command,
respectively, to create it.
 
\subsection*{A Caveat for the \TeX\ Expert}
Because you have just been given permission to use the \texttt{{\char'134}newdef} command to create a new form, you might think you can use \TeX's \texttt{{\char'134}def} to create a new command: \textit{don't you dare!}  Remember that your \LaTeX\ source code is not intended to create camera-ready copy, but to be used for conversion to SGML; that conversion routine is intended for the
structural commands in the acmtrans document class file, and may become deeply
and irreparably confused by structural commands you create.

An example of how to use the {\tt{\char'134}description} environment follows:
\begin{description}
\item[$d$\hskip8pt] the degree of the interpolant;
\item[$n_P$] the number of unknown coefficients for pure bivariate
 interpolation,
$(d+1)(d+2)/2$;
\item[$n_B$]  the number of unknown coefficients for bivariate
interpolation, $(d+1)^2$;
\item[$R_P$]  the maximum number of independent collocation
constraints for pure bivariate interpolation, $d(d+1)/2$;
\item[$R_B$]  the maximum number of independent collocation
constraints for bivariate interpolation, $d^2-(1-(-1)^d)/2$;
\item[$m_P^R$]  the required number of collocation points for
pure bivariate interpolation on rectangles, $(d+1)(d+2)/2-12$;
\item[$m_B^R$]  the required number of collocation points for
bivariate interpolation on rectangles, $(d+1)^2-12$;
\item[$m_P^T$]  the required number of collocation points for
pure bivariate interpolation on triangles, $(d+1)(d+2)/2-9$;
\item[$m_B^T$]  the required number of collocation points
for bivariate interpolation on triangles, $(d+1)^2-9$;
 \end{description}

\section{Conclusions}
This paragraph will end the body of this sample document.  Remember that you
might still have Acknowledgements or Appendices; brief samples of these
follow.  There is still the Bibliography to deal with; and we will make a
disclaimer about that here: with the exception of the reference to the
\LaTeX\ book, the citations in this paper are to articles which are interesting
but which have nothing to do with the present subject.  They are used as
examples only.

%ACKNOWLEDGEMENTS are optional
\begin{acks}
The authors thank Bernard M.E. Moret for his work on preparing the \textbf{esub2acm.cls}
and \textbf{esub2acm.bst} files.
We would also like to thank Gerald Murray of ACM for his help in codifying the
\textit{Author's Guide}.
\end{acks}
%APPENDICES are optional
\appendix
%Appendix A
\section{How Hierarchical Headings Change in Appendices}
The rules about hierarchical headings discussed above for the body of the
article are different in the appendices.  Here, \textbf{section} is used to
indicate the start of the Appendix.  So, if you need hierarchical structure
\textit{within} an Appendix, start with \textbf{subsection} as the highest level.
Here is an outline of the body of this document in Appendix-appropriate form:
\subsection{Introduction}
\subsubsection{Type Changes and Special Characters}
\subsubsection{Math Equations}
\paragraph{Inline (In-text) Equations}
\paragraph{Display Equations}
\subsubsection{Citations}
\subsubsection{Tables}
\subsubsection{Figures}
\subsubsection{Theorem-like Constructs}
\subsubsection*{A Caveat for the \TeX\ Expert}
\subsection{Conclusions}
% This next section command marks the start of Appendix B, and does not
% continue the present hierarchy
\section{More Help for the Hardy}
The acmtrans document class file itself is chock-full of succinct and helpful
comments.  If you consider yourself a moderately experienced to expert
user of \LaTeX, you may find reading it useful.
%
% The following two commands are all you need in your .tex file to
% produce the bibliography for the citations herein.
% You must have a proper .bib file, and remember to run:
% latex bibtex latex latex
% to resolve all references
%\bibliographystyle{esub2acm}
%\bibliography{smpsub2e}
%
% HOWEVER, having run 'latex bibtex latex latex' and thus, having
% obtained your .bbl file, you should read it into your main
% file (at the end) just like this:
%

\begin{thebibliography}{}

\bibitem[\protect\citeauthoryear{Bowman, Debray, and Peterson}{Bowman
  et~al.}{1993}]{bowman:reasoning}
\bibsc{Bowman, M., Debray, S.~K., and Peterson, L.~L.} \bibyear{1993}.
\newblock Reasoning about naming systems.
\newblock \bibemphic{ACM Trans. Program. Lang. Syst.}~\bibemph{15},~5
  (November), 795--825.

\bibitem[\protect\citeauthoryear{Braams}{Braams}{1991}]{braams:babel}
\bibsc{Braams, J.} \bibyear{1991}.
\newblock Babel, a multilingual style-option system for use with latex's
  standard document styles.
\newblock \bibemphic{TUGboat}~\bibemph{12},~2 (June), 291--301.

\bibitem[\protect\citeauthoryear{Herlihy}{Herlihy}{1993}]{herlihy:methodology}
\bibsc{Herlihy, M.} \bibyear{1993}.
\newblock A methodology for implementing highly concurrent data objects.
\newblock \bibemphic{ACM Trans. Program. Lang. Syst.}~\bibemph{15},~5
  (November), 745--770.

\bibitem[\protect\citeauthoryear{Lamport}{Lamport}{1986}]{Lamport:LaTeX}
\bibsc{Lamport, L.} \bibyear{1986}.
\newblock \bibemph{LaTeX User's Guide and Document Reference Manual}.
\newblock Addison-Wesley Publishing Company, Reading, Massachusetts.

\bibitem[\protect\citeauthoryear{Salas and Hille}{Salas and
  Hille}{1978}]{salas:calculus}
\bibsc{Salas, S. and Hille, E.} \bibyear{1978}.
\newblock \bibemph{Calculus: One and Several Variable}.
\newblock John Wiley and Sons, New York.

\end{thebibliography}

\end{document}
