# Makefile
#
# DO NOT USE THIS ANYMORE; SEE ~/lib/Makefile.defaults
#

# configure these to the local directory structure

# location of the library
# LIBDIR = /Users/jhh/lib/shared
LIBDIR = ./lib
# location of the external library
# EXTERNALSDIR = /Users/jhh/externals
EXTERNALSDIR = ./externals
# location for font files
FONTSDIR = $(EXTERNALSDIR)//:$(LIBDIR)/fonts//
# location for tex/latex macros
MACROSDIR = $(EXTERNALSDIR)//:$(LIBDIR)/tex//
# location for .bib files and bibtex style files
BIBDIR = $(EXTERNALSDIR)//:$(LIBDIR)/tex//
# basename of target latex file
TARGET = name
# Use pdflatex
#PDFLATEX = yes
# Use biber
#BIBER = yes
# Prefer entries in local .bib file instead of overwriting with entries in
# remote .bib files
#BIBKEEP = yes

include $(LIBDIR)/Makefile.main

# include other dependencies here (eg figures etc)
#
# for example a.dvi: b.tex c.tex


